﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] courseId = { 1234, 4567, 7891, 2345, 5678, 8912, 2345, 6788, 7912, 6219 };
            int[] tempCourseId = courseId;

            Console.Write("Original Array 1: ");

            foreach (int i in courseId)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();

            string[] courseName = { "Business", "Hospitality", "Computing", "Cooking", "Horticulture", "Nursing", "Teaching", "Accounting", "Zooology", "Biology" };
            string[] tempCourseName = courseName;

            Console.Write("Original Array 2: ");

            foreach (string i in courseName)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();

            //Display Array Length
            Console.WriteLine();
            Console.WriteLine($"Length of Array 1 = {tempCourseId.Length}");
            Console.WriteLine($"Length of Array 2 = {tempCourseName.Length}");
            Console.WriteLine();

            //Copy the Array
            int[] tempCourseIdCopy = new int[10];
            Array.Copy(tempCourseId, tempCourseIdCopy, 10);
            Console.Write("Display copied Array 1: ");
            foreach (int i in tempCourseIdCopy)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();

            string[] tempCourseNameCopy = new string[10];
            Array.Copy(tempCourseName, tempCourseNameCopy, 10);
            Console.Write("Display copied Array 2: ");
            foreach (string i in tempCourseNameCopy)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();

            //Get type of Array
            Console.Write("Array 1 is of Type: ");
            Console.Write(tempCourseIdCopy.GetType());
            Console.WriteLine();

            Console.Write("Array 2 is of Type: ");
            Console.Write(tempCourseNameCopy.GetType());
            Console.WriteLine();

            //get value of array at index 5
            Console.Write("Index 5 of Array 1: ");
            Console.Write(tempCourseIdCopy.GetValue(5));
            Console.WriteLine();
            Console.Write("Index 5 of Array 2: ");
            string index5 = tempCourseNameCopy.GetValue(5).ToString();
            Console.Write(index5);
            Console.WriteLine();
           

            //Reverse the Array
            Array.Reverse(tempCourseId);
            Console.WriteLine();
            Console.Write("Reversed Array 1: ");

            foreach (int i in tempCourseId)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();

            Array.Reverse(tempCourseName);
            Console.Write("Reversed Array 2: ");

            foreach (string i in tempCourseName)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();

            //Sort the Array 
            Array.Sort(tempCourseId);
            Console.Write("Sorted Array 1: ");

            foreach (int i in tempCourseId)

            Console.ReadLine();


        }
    }
}
